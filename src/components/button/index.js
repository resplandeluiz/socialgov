import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'

import styles from './styles'

const ButtonComponent = ({ title, onPress, disabled = false, layout = {} }) => {
    return (
        <TouchableOpacity onPress={onPress} disabled={disabled}>
            <View style={[styles.buttonContainer, layout]}>
                <Text style={styles.buttonText}>{title}</Text>
            </View>
        </TouchableOpacity>

    )
}

export default ButtonComponent;