import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    buttonContainer: {
        width: 200,
        borderRadius: 10,
        paddingVertical: 10,
        paddingHorizontal: 20,
        backgroundColor: "#FFFFFF",
        alignItems: "center",
    },
    buttonText: {
        textAlign: "center",
        fontWeight: "bold",
        fontSize: 20,
        color: "#1F91FF"
    }
})

export default styles;

