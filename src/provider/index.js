import React, { useState, createContext } from 'react'

export const DataContext = createContext()

export const Provider = ({ children }) => {

    const [userData, setUserData] = useState(null)

    return (
        <DataContext.Provider
            value={{
                userData,
                addUser: data => setUserData(data)                
            }}
        >
            {children}
        </DataContext.Provider>
    )
}
