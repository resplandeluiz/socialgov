import React from 'react'

import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import { Provider } from './provider'

//Screens
import Login from './views/login'
import Home from './views/home'
import EmergencyList from './views/emergencyList'
import EmergencyItem from './views/emergencyDetail'
import Chat from './views/chat'

const Stack = createStackNavigator()

const App = () => {
  return (
    <NavigationContainer>

      <Provider>

        <Stack.Navigator initialRouteName={"Login"}>

          <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
          <Stack.Screen name="Home" component={Home} options={{ headerShown: false }} />
          <Stack.Screen name="EmergencyList" component={EmergencyList} options={{ title: "Serviços de emergência" }} />
          <Stack.Screen name="EmergencyItem" component={EmergencyItem} />
          <Stack.Screen name="Chat" component={Chat} />

        </Stack.Navigator>

      </Provider>

    </NavigationContainer>
  );
};

export default App;
