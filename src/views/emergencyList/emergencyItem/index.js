import { useNavigation } from '@react-navigation/core'
import React from 'react'

import { Text, View, Image } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { Card, Title } from 'react-native-paper'
import styles from './styles'


const EmergencyItem = ({ item }) => {

    const navigation = useNavigation()

    return (
        <TouchableOpacity style={styles.container} onPress={() => navigation.push("EmergencyItem", { item: JSON.stringify(item) })}>
            <Card >
                <Card.Content style={styles.cardContent}>
                    <Title>{item.name}</Title>
                    <Image source={item.user_Icone} style={styles.image} />
                </Card.Content>
            </Card>
        </TouchableOpacity>
    )
}

export default EmergencyItem