import { StyleSheet } from "react-native"

export default styles = StyleSheet.create({
    container: {
        flex: 1,
        marginHorizontal: 20,
        padding: 10
    },

    cardContent: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center"
    },
    image: {
        width: 80, height: 50, resizeMode: "contain"
    }
})