import React from 'react'

import { Text, FlatList, View } from 'react-native'
import styles from './styles'

import { services } from '../../utils/serviceData'

import EmergencyItem from './emergencyItem'

const EmergyList = () => {
    return (
        <View style={styles.container}>
            <FlatList
                data={services}
                renderItem={({ item }) => <EmergencyItem item={item} />}
                keyExtractor={(item) => item.id}
                showsVerticalScrollIndicator={false}
            />
        </View>
    )
}

export default EmergyList