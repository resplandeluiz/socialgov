import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#1F91FF",
        alignItems: "center",
        justifyContent: "center"
    },

    containerInput: {
        flexGrow: 1,
        width: "100%",
        alignItems: "center",
        justifyContent: "center"
    },

    input: {
        fontSize: 20,
        width: "80%",
        borderBottomColor: "rgba(255,255,255,0.6)",
        borderBottomWidth: 2,
        marginBottom: 15,
        paddingHorizontal: 10
    },

    textError: {
        color: "#FFFFFF"
    },

    containerImage: {
        flex: 3,
        width: "100%",
        alignItems: "center",
        justifyContent: "center"
    },

    image: {
        width: 200,
        resizeMode: "contain",
        borderRadius: 10
    }


})

export default styles;

