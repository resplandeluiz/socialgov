import React, { useState, useContext } from 'react'
import { useNavigation } from '@react-navigation/core'
import { TextInput, View, SafeAreaView, Image, Text } from 'react-native'

import styles from './styles'

import ButtonComponent from '../../components/button'
import { DataContext } from '../../provider'

const LOGO_LOGIN = require('../../assets/imagens/logo-home.png')

const Login = () => {
    
    const navigation = useNavigation()
    
    const { addUser } = useContext(DataContext)

    const [userName, setUserName] = useState("")
    const [password, setPassword] = useState("")
    const [error, setError] = useState("")

    const login = (username, password) => {

        if (username == "" || password == "") {
            setError("Não foi possível logar :(")
            return
        }

        setError("");

        addUser({ userName })

        navigation.replace("Home")
    }

    return (
        <SafeAreaView style={styles.container}>

            <View style={styles.containerImage}>
                <Image source={LOGO_LOGIN} style={styles.image} />
            </View>

            <View style={styles.containerInput}>
                <TextInput
                    style={styles.input}
                    placeholder={"Usuário"}
                    onChangeText={(text) => setUserName(text)}

                />

                <TextInput
                    style={styles.input}
                    placeholder={"Senha"}
                    secureTextEntry={true}
                    onChangeText={(text) => setPassword(text)}
                />

                <Text style={styles.textError}>{error}</Text>
            </View>


            <View style={{ flex: 1 }}>
                <ButtonComponent title={"Entrar"} style={styles.button} onPress={() => login(userName, password)} />
            </View>


        </SafeAreaView>
    )
}

export default Login