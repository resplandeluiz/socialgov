import { StyleSheet } from 'react-native'
export default styles = StyleSheet.create({

    container: {
        flex: 1
    },
    containerMessagens: {
        flexDirection: "row",
        padding: 10,
        justifyContent: "space-between"
    },
    footer: {
        flex: 1,
        position: "absolute",
        bottom: 0,
        backgroundColor: "#FFFFFF",
        width: "100%",
        padding: 10,
        justifyContent: "space-between",
        flexDirection: "row",
        alignItems: "center"
    }


})