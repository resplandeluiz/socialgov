import React from 'react'
import { Text, FlatList, View } from 'react-native'

import styles from './styles'
import mensagens from '../../utils/chat'
import { IconButton } from 'react-native-paper';

const Mensagem = ({ item }) => {

    return (
        <View style={styles.containerMessagens}>

            {item.from == "user" && <View />}

            <View style={{
                backgroundColor: item.from == "user" ? "#caefff" : "#fcb762",
                borderTopLeftRadius: item.from == "user" ? 20 : 0,
                borderBottomLeftRadius: item.from == "user" ? 20 : 0,
                borderTopRightRadius: item.from == "user" ? 0 : 20,
                borderBottomRightRadius: item.from == "user" ? 0 : 20,
                padding: 10,

            }}>
                <Text style={{ textAlign: item.from == "user" ? "right" : "left" }}>
                    {item.messagem}
                </Text>
            </View>
        </View>
    )
}


const Footer = () => {
    return (
        <View style={styles.footer}>
            <Text>Sistema indisponível.</Text>
            <IconButton
                icon={"heart-broken"}
                color={"#000"}
                size={30}
            />
        </View>
    )
}

const Chat = () => {
    return (
        <View style={styles.container}>
            <View style={{ flexGrow: 1 }}>
                <FlatList
                    data={mensagens}
                    renderItem={({ item }) => <Mensagem item={item} />}
                    keyExtractor={(item) => item.id}
                    showsVerticalScrollIndicator={false}
                />
            </View>
            
            <Footer />

        </View>

    )
}
export default Chat