import React from 'react'
import { ImageBackground } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'

import styles from './styles'

const BGHome = () => {

    return (

        <ImageBackground style={styles.background}>
            <LinearGradient
                colors={['#1F91FF', 'white','white', 'white', 'white']}
                style={styles.gradient}
            />
        </ImageBackground>

    )
}

export default BGHome