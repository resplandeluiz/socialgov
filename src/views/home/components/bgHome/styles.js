import { StyleSheet, Dimensions } from 'react-native'

const { width, height } = Dimensions.get("screen")

const styles = StyleSheet.create({
    
    background: {
        position: "absolute",
        width,
        height
    },

    gradient: {
        alignItems: 'center',
        justifyContent: 'center',
        width, height
    }



})

export default styles;

