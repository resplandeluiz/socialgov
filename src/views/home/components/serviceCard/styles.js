import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    cardContainer: {
        marginBottom: 20,
        backgroundColor: "#1F91FF"
    },

    cardContent: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center"
    },

    textTitle: {
        color: "white"
    },

    icon: {
        margin: 0,
        padding: 0
    }

})


export default styles