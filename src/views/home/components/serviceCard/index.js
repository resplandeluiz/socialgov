import React from 'react'
import { useNavigation } from '@react-navigation/core'
import { Card, Title } from 'react-native-paper'
import { TouchableOpacity,  } from 'react-native'

import styles from './styles';
import { IconButton } from 'react-native-paper';



const ServiceCard = ({ title, screen, icon }) => {

    const navigation = useNavigation()

    const goToScreen = () => navigation.push(screen)

    return (
        <TouchableOpacity onPress={() => goToScreen()}>
            <Card style={styles.cardContainer}>
                <Card.Content style={styles.cardContent}>
                    <Title style={styles.textTitle}>{title}</Title>

                    <IconButton
                        style={styles.icon}
                        icon={icon}
                        color={"#FFFFFF"}
                        size={30}
                    />
                </Card.Content>
            </Card>
        </TouchableOpacity>
    )
}

export default ServiceCard;