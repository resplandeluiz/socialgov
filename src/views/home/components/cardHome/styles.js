import { StyleSheet, Dimensions } from 'react-native'

const { width, height } = Dimensions.get("screen")

const styles = StyleSheet.create({

    containerCard: {
        marginBottom: 20,
        width: width,
        paddingHorizontal: 30        
    },

    containerLinkPerfil: {
        alignItems: "center",
        justifyContent: "center",
    },

    cardContent: {
        alignItems: "center",
        justifyContent: "space-between",
        flexDirection: "row"
    },

    boxImage: {
        borderColor: "#1F91FF",
        borderWidth: 3,
        borderRadius: 60
    },

    image: {
        width: 80,
        height: 80,
        resizeMode: "contain"
    },
    boxLine: {
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
        marginTop: 40,
        marginBottom: 20
    },

    line: {
        borderBottomColor: '#1F91FF',
        borderBottomWidth: 3,
        width: "80%",
    }
})


export default styles