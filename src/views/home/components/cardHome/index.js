import React from 'react'

import { View, Image, Text, TouchableOpacity, Animated } from 'react-native'
import { Card, Title } from 'react-native-paper';

import styles from './styles';

const IMG_USER = require('../../../../assets/icon-users/user-example.png')

const CardHome = ({ nome }) => {

    return (
        <View style={styles.containerCard}>
            <Card style={{ borderRadius: 30 }}>

                <Card.Content style={styles.cardContent}>


                    <View style={styles.boxDetail}>
                        <Title>{nome}</Title>
                        <Text>Ceilândia - DF</Text>
                    </View>


                    <View style={styles.boxImage}>
                        <Image source={IMG_USER} style={styles.image} />
                    </View>

                </Card.Content>
                {/* 
                <View style={styles.boxLine}>
                    <View style={styles.line} />
                </View>

                <Card.Content style={styles.containerLinkPerfil}>
                    <TouchableOpacity>
                        <Title>Ver meu perfil</Title>
                    </TouchableOpacity>
                </Card.Content> */}
            </Card>
        </View>
    )
}

export default CardHome;