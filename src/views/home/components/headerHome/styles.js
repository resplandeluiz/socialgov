import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({

    containerHeader: {
        width: "80%",
        padding: 10,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center"
    },

    textHeader: {
        fontSize: 40,
        fontWeight: "bold",
        color: "#FFFFFF"
    }

})

export default styles;

