import React  from 'react'

import { useNavigation } from '@react-navigation/core'
import { Text, TouchableOpacity, View } from 'react-native'

import styles from './styles';
import { IconButton } from 'react-native-paper';

const HeaderHome = ({ onPress }) => {

    const navigation = useNavigation()

    const deslogar = () => navigation.replace("Login")

    return (
        <View style={styles.containerHeader}>

            <View>
                <Text style={styles.textHeader}>Social Gov</Text>
            </View>

            <TouchableOpacity onPress={onPress}>

                <IconButton
                    icon="exit-to-app"
                    color={"#FFFFFF"}
                    size={30}
                    onPress={() => deslogar()}
                />
            </TouchableOpacity>

        </View>
    )
}

export default HeaderHome;