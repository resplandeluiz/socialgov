import React, { useContext } from 'react'
import { SafeAreaView, View, } from 'react-native'
import { DataContext } from '../../provider'
import { Title } from 'react-native-paper'
import styles from './styles'

import HeaderHome from './components/headerHome'
import CardHome from './components/cardHome'
import BGHOMe from './components/bgHome'
import ServiceCard from './components/serviceCard'


const Home = () => {

    const { userData, logout } = useContext(DataContext)

    return (
        <>

            <BGHOMe />

            <SafeAreaView style={styles.container}>

                <HeaderHome />

                <CardHome nome={userData.userName} />

                <View style={{ flex: 1, width: "100%", paddingHorizontal: 30, borderRadius: 30, }}>

                    <View style={{ marginBottom: 20 }}>
                        <Title>Serviços</Title>
                    </View>

                    <ServiceCard title={"Serviços de emergência"} screen={"EmergencyList"} icon={"alert-circle"} />

                    <ServiceCard title={"Consulta popular"} screen={"Chat"} icon={"chat"} />

                </View>

                <View style={{ flex: 1 }}>

                </View>


            </SafeAreaView>
        </>
    )
}

export default Home