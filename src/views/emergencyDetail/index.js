import React from 'react'
import { useNavigation, StackActions, useRoute } from '@react-navigation/core'
import { View } from 'react-native'

import styles from './styles'


import BackgroundImage from './backgroundImage'
import EmergencyData from './emergencyData'


const EmergencyDetail = ({ route }) => {

    const item = JSON.parse(route.params.item)

    return (

        <View style={styles.container}>
            <BackgroundImage image={item.user_Icone} />
            <EmergencyData  {...item} />
        </View>

    )
}

export default EmergencyDetail