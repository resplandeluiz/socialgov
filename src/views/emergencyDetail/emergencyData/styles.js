import { StyleSheet } from "react-native"

export default styles = StyleSheet.create({
    container: {
        flex: 5,
    },
    boxButton: {
        alignItems: "center",
        justifyContent: "center",
        marginTop: -20,
        marginBottom: 20
    },
    boxContent: {
        marginHorizontal: 24
    }

})