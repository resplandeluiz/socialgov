import { useNavigation } from '@react-navigation/core'
import React from 'react'

import { Text, View, Image } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { Card, Paragraph, Title } from 'react-native-paper'
import styles from './styles'
import Button from '../../../components/button'

const EmergencyData = ({ name, numero, desc }) => {
    return (
        <View style={styles.container}>
            <View style={styles.boxButton}>
                <Button title={"Chamar emergência"} onPress={() => { alert("Apenas aguarde !") }} layout={{ borderRadius: 40, }} />
            </View>

            <View style={styles.boxContent}>
                <Title>{name} - {numero}</Title>


                <Paragraph>{desc}</Paragraph>
            </View>

        </View>
    )
}

export default EmergencyData