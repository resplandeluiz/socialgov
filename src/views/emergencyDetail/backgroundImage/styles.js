import { StyleSheet } from "react-native"

export default styles = StyleSheet.create({
    container: {
        flex: 5,
        backgroundColor: "#1F91FF",
        alignItems: "center",
        justifyContent: "center"

    },
    containerImage: {
        borderWidth: 10,
        borderColor: "rgba(255,255,255,0.6)",
        borderRadius: 100,
        shadowColor: "#fff",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,
        elevation: 8,
    },

    image: {
        width: 150,
        height: 140,
        resizeMode: "contain"
    }

})