import React from 'react'
import { Text, View, Image } from 'react-native'
import { Title } from 'react-native-paper'

import styles from './styles'


const BackgroundImage = ({ image }) => {

    return (
        <View style={styles.container}>
            <View style={styles.containerImage}>
                <Image source={image} style={styles.image} />                
            </View>
        </View>
    )
}

export default BackgroundImage