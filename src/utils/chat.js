const mensagens = [
    {
        id: "1_user",
        from: "user",
        messagem: "Olá, quero saber oque fazer quando tenho que despejar um hóspede."
    },
    {
        id: "1_bot",
        from: "bot",
        messagem: "Oi, tudo bem ?"
    },
    {
        id: "2_bot",
        from: "bot",
        messagem: "O despejo é uma ação específica realizada pelo proprietário de um imóvel que está alugado. O objetivo principal desse tipo de ação é viabilizar a desocupação do bem em questão, retomando a posse total para o dono. Esse tipo de ação recebe esse nome porque obriga o inquilino a deixar o imóvel pelas mais diversas razões."
    },
    {
        id: "2_user",
        from: "user",
        messagem: "Obrigado!"
    }
]

export default mensagens