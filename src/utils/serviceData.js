export const services = [
        {
                "id": "police_service",
                "name": "Serviço policial",
                "numero": "190",
                "user_Icone": require("../assets/icon-users/police.png"),
                "desc": "Desencorajar a criminalidade e proteger aa comunidade através de um policiamento de alta visibilidade. Patrulhamento das áreas específicas e monitoramento das atividades para proteger pessoas/propriedades. Investigação de crimes e prisão de suspeitos de violação da lei."
        },
        {
                "id": "firefight_service",
                "name": "Corpo de bombeiros",
                "numero": "193",
                "user_Icone": require("../assets/icon-users/firefight.png"),
                "desc": "Suas principais funções são: prevenção e combate a incêndio, situações de pânico, busca e salvamento de pessoas e defesa civil."
        },
        {
                "id": "medic_service",
                "name": "SAMU",
                "numero": "192",
                "user_Icone": require("../assets/icon-users/medic.png"),
                "desc": "O SAMU é o Serviço de Atendimento Móvel de Urgência, que atende os casos de urgência e emergência, financiado pelo Governo Federal, Estadual e Municipais, com a finalidade de melhorar o atendimento a população. Foi criado em 2003 e faz parte do Política Nacional de Urgências e Emergências."
        },
        {

                "id": "defence_civil",
                "name": "Defesa Civil",
                "numero": "199",
                "user_Icone": require("../assets/icon-users/defence-civil.png"),
                "desc": "A defesa civil ou proteção civil (AO 1945: protecção civil) é o conjunto de ações preventivas, de socorro, assistenciais e reconstrutivas destinadas a evitar ou minimizar os desastres naturais e os incidentes tecnológicos, preservar a moral da população e restabelecer a normalidade social."
        },

]